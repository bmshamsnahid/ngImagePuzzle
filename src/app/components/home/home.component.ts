import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  imagePath: String[] = [
    'assets/images/image_zero.png',
    'assets/images/image_one.png',
    'assets/images/image_two.png',
    'assets/images/image_three.png',
    'assets/images/image_four.png',
    'assets/images/image_five.png',
    'assets/images/image_six.png',
    'assets/images/image_seven.png',
    'assets/images/image_eight.png',
    'assets/images/image_nine.png',
  ];

  imageContainer: String[] = [];
  imageContainerFlag: Boolean[] = [false, false, false, false, false, false, false, false, false];

  selectedWindow: String;
  currentEmptyPosition: number;

  myWidth = '100px';
  outsetBorderImage00 = false;
  outsetBorderImage01 = false;
  outsetBorderImage02 = false;
  outsetBorderImage03 = false;
  outsetBorderImage04 = false;
  outsetBorderImage05 = false;
  outsetBorderImage06 = false;
  outsetBorderImage07 = false;
  outsetBorderImage08 = false;

  constructor() { }

  ngOnInit() {
    console.log('In the ngOnIt');
    let myArray = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    myArray = this.shuffle(myArray);
    for (let i = 0; i < 9; i++) {
      if (myArray[i] === 8) {
        this.currentEmptyPosition = i;
      }
      this.imageContainer[i] = this.imagePath[myArray[i]];
    }
    this.checkPosition();
   }

  onSelectExtraSmallDevice() {
    this.myWidth = '25px';
  }

  onSelectSmallDevice() {
    this.myWidth = '50px';
  }

  onSelectStandardDevice() {
    this.myWidth = '100px';
  }

  onSelectLargeDevice() {
    this.myWidth = '200px';
  }

  onClickImage00() {
    if (this.isAdjacent(0) === true) {
      console.log('Adjacent');
      this.imageContainer[this.currentEmptyPosition] = this.imageContainer[0];
      this.imageContainer[0] = this.imagePath[8];
      this.currentEmptyPosition = 0;
    } else {
      console.log('Not Adjacent');
    }

    if (this.checkPosition()) {
      console.log('Game Finished');
    } else {
      console.log('Not Finished');
    }

    this.outsetBorderImage00 = !this.outsetBorderImage00;
    this.outsetBorderImage01 = false;
    this.outsetBorderImage02 = false;
    this.outsetBorderImage03 = false;
    this.outsetBorderImage04 = false;
    this.outsetBorderImage05 = false;
    this.outsetBorderImage06 = false;
    this.outsetBorderImage07 = false;
    this.outsetBorderImage08 = false;
  }
  onClickImage01() {
    if (this.isAdjacent(1) === true) {
      console.log('Adjacent');
      this.imageContainer[this.currentEmptyPosition] = this.imageContainer[1];
      this.imageContainer[1] = this.imagePath[8];
      this.currentEmptyPosition = 1;
    } else {
      console.log('Not Adjacent');
    }

    if (this.checkPosition()) {
      console.log('Game Finished');
    } else {
      console.log('Not Finished');
    }

    this.outsetBorderImage00 = false;
    this.outsetBorderImage01 = !this.outsetBorderImage01;
    this.outsetBorderImage02 = false;
    this.outsetBorderImage03 = false;
    this.outsetBorderImage04 = false;
    this.outsetBorderImage05 = false;
    this.outsetBorderImage06 = false;
    this.outsetBorderImage07 = false;
    this.outsetBorderImage08 = false;
  }
  onClickImage02() {
    if (this.isAdjacent(2) === true) {
      console.log('Adjacent');
      this.imageContainer[this.currentEmptyPosition] = this.imageContainer[2];
      this.imageContainer[2] = this.imagePath[8];
      this.currentEmptyPosition = 2;
    } else {
      console.log('Not Adjacent');
    }

    if (this.checkPosition()) {
      console.log('Game Finished');
    } else {
      console.log('Not Finished');
    }

    this.outsetBorderImage00 = false;
    this.outsetBorderImage01 = false;
    this.outsetBorderImage02 = !this.outsetBorderImage02;
    this.outsetBorderImage03 = false;
    this.outsetBorderImage04 = false;
    this.outsetBorderImage05 = false;
    this.outsetBorderImage06 = false;
    this.outsetBorderImage07 = false;
    this.outsetBorderImage08 = false;
  }
  onClickImage03() {
    if (this.isAdjacent(3) === true) {
      console.log('Adjacent');
      this.imageContainer[this.currentEmptyPosition] = this.imageContainer[3];
      this.imageContainer[3] = this.imagePath[8];
      this.currentEmptyPosition = 3;
    } else {
      console.log('Not Adjacent');
    }

    if (this.checkPosition()) {
      console.log('Game Finished');
    } else {
      console.log('Not Finished');
    }

    this.outsetBorderImage00 = false;
    this.outsetBorderImage01 = false;
    this.outsetBorderImage02 = false;
    this.outsetBorderImage03 = !this.outsetBorderImage03;
    this.outsetBorderImage04 = false;
    this.outsetBorderImage05 = false;
    this.outsetBorderImage06 = false;
    this.outsetBorderImage07 = false;
    this.outsetBorderImage08 = false;
  }
  onClickImage04() {
    if (this.isAdjacent(4) === true) {
      console.log('Adjacent');
      this.imageContainer[this.currentEmptyPosition] = this.imageContainer[4];
      this.imageContainer[4] = this.imagePath[8];
      this.currentEmptyPosition = 4;
    } else {
      console.log('Not Adjacent');
    }

    if (this.checkPosition()) {
      console.log('Game Finished');
    } else {
      console.log('Not Finished');
    }

    this.outsetBorderImage00 = false;
    this.outsetBorderImage01 = false;
    this.outsetBorderImage02 = false;
    this.outsetBorderImage03 = false;
    this.outsetBorderImage04 = !this.outsetBorderImage04;
    this.outsetBorderImage05 = false;
    this.outsetBorderImage06 = false;
    this.outsetBorderImage07 = false;
    this.outsetBorderImage08 = false;
  }
  onClickImage05() {
    if (this.isAdjacent(5) === true) {
      console.log('Adjacent');
      this.imageContainer[this.currentEmptyPosition] = this.imageContainer[5];
      this.imageContainer[5] = this.imagePath[8];
      this.currentEmptyPosition = 5;
    } else {
      console.log('Not Adjacent');
    }

    if (this.checkPosition()) {
      console.log('Game Finished');
    } else {
      console.log('Not Finished');
    }

    this.outsetBorderImage00 = false;
    this.outsetBorderImage01 = false;
    this.outsetBorderImage02 = false;
    this.outsetBorderImage03 = false;
    this.outsetBorderImage04 = false;
    this.outsetBorderImage05 = !this.outsetBorderImage05;
    this.outsetBorderImage06 = false;
    this.outsetBorderImage07 = false;
    this.outsetBorderImage08 = false;
  }
  onClickImage06() {
    if (this.isAdjacent(6) === true) {
      console.log('Adjacent');
      this.imageContainer[this.currentEmptyPosition] = this.imageContainer[6];
      this.imageContainer[6] = this.imagePath[8];
      this.currentEmptyPosition = 6;
    } else {
      console.log('Not Adjacent');
    }

    if (this.checkPosition()) {
      console.log('Game Finished');
    } else {
      console.log('Not Finished');
    }

    this.outsetBorderImage00 = false;
    this.outsetBorderImage01 = false;
    this.outsetBorderImage02 = false;
    this.outsetBorderImage03 = false;
    this.outsetBorderImage04 = false;
    this.outsetBorderImage05 = false;
    this.outsetBorderImage06 = !this.outsetBorderImage06;
    this.outsetBorderImage07 = false;
    this.outsetBorderImage08 = false;
  }
  onClickImage07() {
    if (this.isAdjacent(7) === true) {
      console.log('Adjacent');
      this.imageContainer[this.currentEmptyPosition] = this.imageContainer[7];
      this.imageContainer[7] = this.imagePath[8];
      this.currentEmptyPosition = 7;
    } else {
      console.log('Not Adjacent');
    }

    if (this.checkPosition()) {
      console.log('Game Finished');
    } else {
      console.log('Not Finished');
    }

    this.outsetBorderImage00 = false;
    this.outsetBorderImage01 = false;
    this.outsetBorderImage02 = false;
    this.outsetBorderImage03 = false;
    this.outsetBorderImage04 = false;
    this.outsetBorderImage05 = false;
    this.outsetBorderImage06 = false;
    this.outsetBorderImage07 = !this.outsetBorderImage07;
    this.outsetBorderImage08 = false;
  }
  onClickImage08() {
    if (this.isAdjacent(8) === true) {
      console.log('Adjacent');
      this.imageContainer[this.currentEmptyPosition] = this.imageContainer[8];
      this.imageContainer[8] = this.imagePath[8];
      this.currentEmptyPosition = 8;
    } else {
      console.log('Not Adjacent');
    }

    if (this.checkPosition()) {
      console.log('Game Finished');
    } else {
      console.log('Not Finished');
    }

    this.outsetBorderImage00 = false;
    this.outsetBorderImage01 = false;
    this.outsetBorderImage02 = false;
    this.outsetBorderImage03 = false;
    this.outsetBorderImage04 = false;
    this.outsetBorderImage05 = false;
    this.outsetBorderImage06 = false;
    this.outsetBorderImage07 = false;
    this.outsetBorderImage08 = !this.outsetBorderImage08;
  }

  shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  isAdjacent(selectedImagePosition): boolean {
    if (selectedImagePosition === 0) {
      if ((this.currentEmptyPosition === selectedImagePosition + 1) ||
        (this.currentEmptyPosition === selectedImagePosition + 3)) {
          return true;
      }
    } else if (selectedImagePosition === 1) {
      if ((this.currentEmptyPosition === selectedImagePosition - 1) ||
      (this.currentEmptyPosition === selectedImagePosition + 1) ||
      (this.currentEmptyPosition === selectedImagePosition + 3)) {
        return true;
      }
    } else if (selectedImagePosition === 2) {
      if ((this.currentEmptyPosition === selectedImagePosition - 1) ||
      (this.currentEmptyPosition === selectedImagePosition + 3)) {
        return true;
      }
    } else if (selectedImagePosition === 3) {
      if ((this.currentEmptyPosition === selectedImagePosition + 1) ||
      (this.currentEmptyPosition === selectedImagePosition + 3) ||
      (this.currentEmptyPosition === selectedImagePosition - 3)) {
        return true;
      }
    } else if (selectedImagePosition === 4) {
      if ((this.currentEmptyPosition === selectedImagePosition + 1) ||
      (this.currentEmptyPosition === selectedImagePosition - 1) ||
      (this.currentEmptyPosition === selectedImagePosition + 3) ||
      (this.currentEmptyPosition === selectedImagePosition - 3)) {
        return true;
      }
    } else if (selectedImagePosition === 5) {
      if ((this.currentEmptyPosition === selectedImagePosition + 3) ||
      (this.currentEmptyPosition === selectedImagePosition - 3) ||
      (this.currentEmptyPosition === selectedImagePosition - 1)) {
        return true;
      }
    } else if (selectedImagePosition === 6) {
      if ((this.currentEmptyPosition === selectedImagePosition + 1) ||
      (this.currentEmptyPosition === selectedImagePosition - 3)) {
        return true;
      }
    } else if (selectedImagePosition === 7) {
      if ((this.currentEmptyPosition === selectedImagePosition - 1) ||
      (this.currentEmptyPosition === selectedImagePosition + 1) ||
      (this.currentEmptyPosition === selectedImagePosition - 3)) {
        return true;
      }
    } else if (selectedImagePosition === 8) {
      if ((this.currentEmptyPosition === selectedImagePosition - 1) ||
      (this.currentEmptyPosition === selectedImagePosition - 3)) {
        return true;
      }
    }
    return false;
  }

  checkPosition(): boolean {
    for (let i = 0; i < 9; i++) {
      console.log(this.imageContainer[i]);
      if (this.imageContainer[i] !== this.imagePath[i]) {
        return false;
      }
    }
    this.imageContainer[8] = this.imagePath[9];
    return true;
  }

}
